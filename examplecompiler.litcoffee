Example compiler

Utility that reads a file, line by line, and creates another file following the given rules.

    fs = require 'fs'
    _ = require 'underscore'


The first argument argv[2] is the name of the file to read:

    inputFileName = process.argv[2]

The second argument argv[3] is the name of the file to write:

    outputFileName = process.argv[3]
    fs.writeFileSync(outputFileName, "")

*append* is a shortcut to append a line to the output file.

    append = (string) ->
        fs.appendFileSync(outputFileName, string)

A *Rule* is a function that takes an input line and a mode, returning
a new state to pass to the next line read, and a mode to be used from then on.

    Rules = []

    append "example = require \"" + __dirname + "/minitest\"\n"

    thenStep = (line) ->
        if /( is )/.test(line)
            "    example.then" + line.replace(/( is )/, ", ") + "\n"
        else
            line + "\n"

    Rule = (pattern, f) ->
        Rules.push (line) ->
            match = pattern.exec(line)
            if match then f.apply(null,_.rest(match))

    exampleMode    = off
    thenMode       = off
    lastLine       = ""
    definitionMode = off
    newLines       = 0

    Rule /^In the examples:(.*)$/, (x) ->
        definitionMode = on
        x

    Rule /^ex: given (.*)$/, (x) ->
        exampleMode = on
        "example \"" + lastLine + "\", -> \n    " + x

    Rule /^\s*$/, (x) ->
        newLines = newLines + 1
        exampleMode = off
        definitionMode = off
        thenMode = off
        ""

    Rule /^\s*then\s(.*)\sis\s(.*)$/, (x,y) ->
        if exampleMode is on
            thenMode = on
            "    example.then " + x + ", " + y

    Rule /^\s*and\s(.*)\sis\s(.*)$/, (x,y) ->
        if exampleMode is on and thenMode is on
            "    example.then " + x + ", " + y

    Rule /^\s*and\s(.*)$/, (x) ->
        if exampleMode is on
            if thenMode is on
                "    example.then " + x
            else
                "    " + x

    Rule /^\s*given\s(.*)$/, (x) ->
        if exampleMode is on
            "    " + x

    Rule /^([^\s].*)$/, (x) ->
        if newLines > 0 then lastLine = ""
        newLines = 0
        lastLine = lastLine + " " + x
        ""

    Rule /^\s\s(.*)$/, (x) ->
        if definitionMode is on then x else ""

    parseLine = (line) ->
        translation = null
        _.find(Rules, (rule)->
            translation = rule(line)
            translation is "" or translation)
        if translation isnt "" then append translation + "\n"

    fs.readFileSync(inputFileName).toString().split('\n').forEach parseLine

    append "example.run()"