To browser browserify -t coffeeify example.litcoffee > example.browser.js

example.litcoffee
A minimalistic testing library
By Jerome Morin-Drouin - jmdrouin@gmail.com - (c) 2013

    assert = require "assert"

    step = (result, string) ->
        this.steps.push result:result, string:string

    example = do ->
        (name, body) ->
            testInstance = { step: step, error: null, name: name, steps: [] }
            try
                body.apply(testInstance)
            catch error
                testInstance.error = error

            console.log testInstance
            testInstance



    module.exports = example

Test the result:

    example "SOMETHING HAPPENS", ->
        this.step(a = 1 + 1, "a = 1 + 1")
        this.step(b = 2, "b = 2")
        this.step( assert(false) , "assert(false)")