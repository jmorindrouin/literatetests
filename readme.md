Literate tests
==============

Copyright (c) 2013 Jérôme Morin-Drouin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

To do
--------------

* fix bug: test only fails on the last 'then' assertion
* cleanup: use a nice when-and-then library
* syntax: dont parse line by line, to allow steps in several lines
* syntax: improve "in the examples" pattern (normal indentation)

Test results
* Print results in a browser
* Build a list of tests so they can run for all files in project
* Duration of tests
* Show result of each line

Tests by sampling
* Add seed and random generation
* Simple library to create objects randomly

Testing tools
* Approximately equal, less/greater
* Test step by step (inside a