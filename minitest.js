(function() {
    var root = this;
    var _ = root._ || require("underscore");

    var cue = [];

    var minitest = function(name, test) {
        if (test===undefined) {
            test = name;
            name = ""
        }else{
            name = " (" + name + ")"
        }
        name = "ex.#" + (_.size(cue)+1) + name;
        cue.push([name,test]);
    }

    minitest.run = function(value) {
        if (value===false) {
            cue = []
            return
        }

        _.each(cue, function(test) {
            var name = test[0];
            var result = test[1]();
            console.log(name, result ? "... ok" : "...FAILED") })}

    minitest.then = function(left, right) {
        if (_.isEqual(left, right)) {
            return true
        }else{
            console.log (left + " != " + right);
        }
    }

    minitest.example = function() {
        args = _.toArray(arguments);
        minitest(args[0], function() {args[1].apply(root,_.rest(args,2))});
    }

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = minitest }
        exports.minitest = minitest;
    }else{
        root.minitest = minitest }

}).call(this);

